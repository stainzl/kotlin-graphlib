package de.stainhaufen.kombinatorischeOptimierung

import de.stainhaufen.kombinatorischeOptimierung.libary.Edge
import de.stainhaufen.kombinatorischeOptimierung.libary.Graph
import de.stainhaufen.kombinatorischeOptimierung.libary.Settings
import de.stainhaufen.kombinatorischeOptimierung.libary.Vertex
import java.io.File
import java.time.LocalDateTime
import java.util.*
import kotlin.collections.HashSet
import kotlin.system.measureTimeMillis

fun main(args: Array<String>) {
    val settings = Settings(args)
    val graph = Graph()

    lateinit var erg: Set<Set<Vertex>>

    Edge.isDirected = settings.isDirected()

    println("Start: ${LocalDateTime.now()}")

    val readTime = measureTimeMillis {
        var first = true
        File(settings.getFileName()).forEachLine {
            if (first) {
                first = false
                println("Create Vertices: ${measureTimeMillis { graph.generateVertices(it.toInt()) }} ms")
            } else {
                val splited = it.split(" ")
                graph.addEdge(splited[0].toInt(), splited[1].toInt())
            }
        }
    }

    println("Readtime: $readTime ms")
    println("Runtime: ${measureTimeMillis {
        erg = if (!settings.isDirected()) {
            connected(graph)
        } else {
            strongConnected(graph)
        }
    }} ms")

    if (settings.toPrint()) {
        printComponents(erg)
    } else {
        println("Component count: ${erg.size}")
    }
}

fun connected(graph: Graph): Set<HashSet<Vertex>> {
    val visited = HashSet<Vertex>()
    val connectedVertices = HashSet<HashSet<Vertex>>()

    for (i in 0 until graph.getNumVertices()) {
        val currentVertex = graph.getVertex(i) ?: continue
        if (!visited.contains(currentVertex)) {
            visited.add(currentVertex)

            val currentList = HashSet<Vertex>()
            currentList.add(currentVertex)
            currentList.addAll(currentVertex.getAdjacents())

            val secondList = HashSet<Vertex>()
            val test = HashSet<Vertex>()

            test.addAll(currentList)

            while (test.isNotEmpty()) {
                secondList.addAll(currentList)
                test.forEach {
                    if (!visited.contains(it)) {
                        visited.add(it)
                        currentList.addAll(it.getAdjacents())
                    }
                }
                test.addAll(currentList)
                test.removeAll(secondList)
            }

            visited.addAll(currentList)
            connectedVertices.add(currentList)
        }
    }

    return connectedVertices
}

fun strongConnected(graph: Graph): Set<Set<Vertex>> {
    val visited = HashSet<Vertex>()
    val connectedVertices = HashSet<Set<Vertex>>()
    val numberedList = Stack<Vertex>()

    println("second step: ${measureTimeMillis {
        graph.getAllVerticesAsList().forEach {
            numberedList.addAll(numberVertices(it, visited))
        }
    } } ms")

    println("second step: ${measureTimeMillis {
        val visitedReversed = HashSet<Vertex>()

        while (numberedList.isNotEmpty()) {
            val current = numberedList.pop()
            val list = connectedVertices(current, visitedReversed)
            if (list.isNotEmpty()) {
                connectedVertices.add(list.toSet())
            }
        }
    }} ms")

    return connectedVertices
}

fun numberVertices(current: Vertex, visited: HashSet<Vertex>): Stack<Vertex> {
    return if (!visited.contains(current)) {
        visited.add(current)
        val numberedVertices = Stack<Vertex>()
        current.getReachableTo().forEach { numberedVertices.addAll(numberVertices(it, visited)) }
        numberedVertices.add(current)
        numberedVertices
    } else {
        Stack()
    }
}

fun connectedVertices(current: Vertex, visited: HashSet<Vertex>): List<Vertex> {
    return if (!visited.contains(current)) {
        visited.add(current)
        val reachable = mutableListOf<Vertex>()
        current.getReachableFrom().forEach { reachable.addAll(connectedVertices(it, visited)) }
        reachable.add(current)
        reachable
    } else {
        emptyList()
    }
}

fun printComponents(connectedComponents: Set<Set<Vertex>>) {
    connectedComponents.forEachIndexed { index, list ->
        run {
            println("$index")
            list.sortedBy { it.getId() }.forEach { print("${it.getId()} ") }
            println()
            println()
        }
    }
}