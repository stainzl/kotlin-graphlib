package de.stainhaufen.kombinatorischeOptimierung.libary

class Graph {
    private val vertices = HashMap<Int, Vertex>()
    private val edges = HashSet<Edge>()

    fun getNumVertices(): Int {
        return vertices.size
    }

    fun getNumEdge(): Int {
        return edges.size
    }

    fun addVertex(vertex: Vertex) {
        vertices[vertex.getId()] = vertex
    }

    fun getVertex(id: Int): Vertex? = vertices[id]

    fun getAllVerticesAsList(): List<Vertex> = vertices.values.toList()

    fun addEdge(edge: Edge) {
        edges.add(edge)
    }

    fun addEdge(startId: Int, endId: Int) {
        val start = vertices[startId] ?: return
        val end = vertices[endId] ?: return
        addEdge(Edge(start, end))
    }

    fun generateVertices(number: Int) {
        for (i in 0 until number) {
            vertices[i] = Vertex(i)
        }
    }
}