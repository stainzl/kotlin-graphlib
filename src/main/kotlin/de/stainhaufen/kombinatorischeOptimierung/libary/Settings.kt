package de.stainhaufen.kombinatorischeOptimierung.libary

import kotlin.system.exitProcess

class Settings(args: Array<String>) {
    private var directed = false
    private var print = false
    private var fileName = ""

    init {
        var computed = false
        for (i in args.indices) {
            println("$i -> ${args[i]}")
            if (computed) {
                computed = false
                continue
            }

            when (Flags.getByFlagName(args[i])) {
                Flags.DIRECTED -> {
                    directed = if (args.size <= i + 1 || Flags.isFlag(args[i + 1])) {
                        true
                    } else {
                        computed = true
                        args[i + 1].toBoolean()
                    }
                }
                Flags.FILENAME -> {
                    computed = true
                    fileName = args[i+1]
                }
                Flags.PRINT_COMPONENTS -> {
                    print = if (args.size <= i + 1 || Flags.isFlag(args[i + 1])) {
                        true
                    } else {
                        computed = true
                        args[i + 1].toBoolean()
                    }
                }
                null -> {
                    println("${args[i]} is not a flag")
                }
            }
        }

        if (fileName == "") {
            println("--filename could not be empty")
            exitProcess(1)
        }
    }

    fun isDirected() = directed
    fun getFileName() = fileName
    fun toPrint() = print

    private enum class Flags(private val flags: List<String>, private val type: String) {
        DIRECTED(listOf("-d", "--directed"), "Boolean"),
        FILENAME(listOf("-f", "--file", "--filname"), "String"),
        PRINT_COMPONENTS(listOf("-p", "--print"), "Boolean");

        companion object {
            fun getByFlagName(name: String): Flags? {
                return values().firstOrNull() { flag -> flag.flags.contains(name) }
            }

            fun isFlag(name: String): Boolean {
                return values().flatMap { flag -> flag.flags }.contains(name)
            }
        }
    }
}