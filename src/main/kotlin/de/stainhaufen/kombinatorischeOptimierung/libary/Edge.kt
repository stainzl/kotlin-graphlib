package de.stainhaufen.kombinatorischeOptimierung.libary

class Edge(private val start: Vertex, private val end: Vertex) {
    companion object {
        @JvmStatic
        var isDirected = false;
    }
    init {
        start.addEdge(this)
        end.addEdge(this)
    }

    fun isAdjacent(vertex: Vertex): Boolean {
        return start == vertex || end == vertex
    }

    fun getStart() = start
    fun getEnd() = end
}