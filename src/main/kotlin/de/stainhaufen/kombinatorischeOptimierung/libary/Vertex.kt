package de.stainhaufen.kombinatorischeOptimierung.libary

class Vertex(private val id: Int) {
    private val edges = HashSet<Edge>()

    fun addEdge(edge: Edge) {
        edges.add(edge)
    }

    fun getNumEdges(): Int = edges.size

    fun getId(): Int = id

    fun getAdjacents(): List<Vertex> = edges.map { if (it.getStart() == this) it.getEnd() else it.getStart() }

    fun getReachableFrom(): List<Vertex> = edges.mapNotNull { if (it.getEnd() == this) it.getStart() else null }

    fun getReachableTo(): List<Vertex> = edges.mapNotNull { if (it.getStart() == this) it.getEnd() else null }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Vertex

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id
    }


}